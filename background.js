
((proxyHost) => {
    const escapeString = (str) => {
        return str.replace(/(\.)/g, '\\$1');
    };
    const matchProxyHostRegExp = new RegExp(`\\.${escapeString(proxyHost)}(?:/|$)`, 'i');
    const getRedirectUrl = (proxyHost, url, proto, host, q) => {
        const proxySubHost = `${host.replace(/\./ig, '-_-')}.${proxyHost}`;
        return `${url.replace(/^https?:\/\/[^\/]+/, `https://${proxySubHost}`)}`;
    };
    const isProxyHostUrl = (url) => {
        return Boolean((url || '').match(matchProxyHostRegExp));
    };
    const getUrlParts = (url) => {
        const match = (url || '').match(/^(https?):\/\/((?:[^\/]+\.)?([^\/^.]+\.[^\/^.]+))[^?]*(\??)/i);
        if(!match) {
            return match;
        }
        const [mstr, proto, host, baseHost, q] = match;
        return {
            proto,
            host,
            baseHost,
            q
        };
    };
    const restoreHost = (host, proxyHost) => {
        return host.split(`.${proxyHost}`).join('').replace(/\-_\-/ig, '.');
    };
    
    const proxiedTabs = new Map();
    const requests = new Map();

    chrome.tabs.onRemoved.addListener((tabId) => {
        proxiedTabs.delete(tabId);
    });

    chrome.tabs.onUpdated.addListener((tabId, changeInfo) => {
        if(tabId >= 0 && changeInfo.url && proxiedTabs.has(tabId)) {
            const urlParts = getUrlParts(changeInfo.url);
            if(urlParts) {
                const proxiedTab = proxiedTabs.get(tabId);
                const host = restoreHost(urlParts.host, proxyHost);
                if(proxiedTab.host !== host) {
                    proxiedTabs.delete(tabId);
                    chrome.tabs.update(tabId, {url: changeInfo.url.replace(/^https?:\/\/[^\/]+/, `https://${host}`)});
                }
            }
        }
    });

    chrome.webRequest.onBeforeRequest.addListener(({initiator, requestId, url, tabId}) => {  
        if(tabId >= 0) {
            if(!initiator) {
                proxiedTabs.delete(tabId);
            }
            if(requests.has(requestId)) {
                const {url: prevUrl} = requests.get(requestId);
                const prevUrlParts = getUrlParts(prevUrl);
                const urlParts = getUrlParts(url);
                if(prevUrlParts && urlParts && prevUrlParts.baseHost !== urlParts.baseHost) {
                    const {proto, host, q} = prevUrlParts;
                    proxiedTabs.set(tabId, {host, requestId});
                    const redirectUrl = getRedirectUrl(proxyHost, prevUrl, proto, host, q);
                    requests.set(requestId, {redirectUrl, proto});
                    return {redirectUrl};
                }
            } else {
                requests.set(requestId, {url});
            }
        }
    }, {urls: ["http://*/*"]}, ["blocking"]);

    chrome.webRequest.onBeforeRequest.addListener(({initiator, url, tabId, requestId}) => {
        if(tabId >= 0) {
            if(!initiator) {
                proxiedTabs.delete(tabId);
            }
            const urlParts = getUrlParts(url);
            if(urlParts) {
                const {proto, host, q} = urlParts;
                if(proxiedTabs.has(tabId) && !isProxyHostUrl(url)) {
                    const redirectUrl = getRedirectUrl(proxyHost, url, proto, host, q);
                    requests.set(requestId, {redirectUrl, proto});
                    return {redirectUrl};
                } else if(!proxiedTabs.has(tabId) && isProxyHostUrl(url)) {
                    proxiedTabs.set(tabId, {host: restoreHost(host, proxyHost), requestId});
                }
            }
        }
    }, {urls: ["https://*/*"]}, ["blocking"]);

    chrome.webRequest.onBeforeSendHeaders.addListener(({tabId, requestId, url, requestHeaders}) => {
        if(tabId >= 0) {
            if(isProxyHostUrl(url)) {
                const requestData = requests.get(requestId);
                if(requestData && requestData.proto) {
                    requestHeaders.push({
                        name: 'X-Proto',
                        value: requestData.proto
                    });
                }
                requestHeaders.push({
                    name: 'X-Online-Proxy',
                    value: '1'
                });
            }
            return {requestHeaders};
        }
    }, {urls: ["<all_urls>"]}, ["blocking", "requestHeaders", "extraHeaders"]);

    chrome.webRequest.onBeforeRedirect.addListener(({redirectUrl, tabId, requestId}) => {
        if(tabId >= 0 && proxiedTabs.has(tabId) && proxiedTabs.get(tabId).requestId === requestId) {
            if(!isProxyHostUrl(redirectUrl)) {
                proxiedTabs.delete(tabId)
            }
        }
    }, {urls: ["<all_urls>"]});

    chrome.webRequest.onCompleted.addListener(({requestId}) => {
        requests.delete(requestId);
    }, {urls: ["<all_urls>"]});

    const connectionErrors = ['net::ERR_CONNECTION_REFUSED', 'net::ERR_CONNECTION_RESET', 'net::ERR_CONNECTION_TIMED_OUT'];
    chrome.webRequest.onErrorOccurred.addListener(({initiator, requestId, error, url, tabId}) => {
        if(tabId >= 0) {
            requests.delete(requestId);
            if(connectionErrors.includes(error) && !isProxyHostUrl(url)) {
                const urlParts = getUrlParts(url);
                if(urlParts) {
                    const {proto, host, q} = urlParts;
                    if(!initiator) {
                        chrome.tabs.update(tabId, {url: getRedirectUrl(proxyHost, url, proto, host, q)});
                    } else if(!isProxyHostUrl(url)) {
                        chrome.tabs.get(tabId, (tab) => {
                            if(tab && (tab.pendingUrl === url ||tab.url === url)) {                        
                                chrome.tabs.update(tabId, {url: getRedirectUrl(proxyHost, url, proto, host, q)});
                            }
                        });    
                    }
                }
            }
        }
    }, {urls: ["<all_urls>"]});

})('litestore.me');
